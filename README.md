# Gitter to GitLab

## Some Product Lessons…

Andrew Newdigate

--------------------------

## Introduction

![](suprememoocow.jpg)

* **Ex**: Software Engineer: UUNET Internet Africa, Internet Solutions, GlaxoSmithKline, Barclays Capital, Barclays Global Investors, Daiwa Capital Markets, Barclays Wealth
* **Co-founder**: Troupe Technologies aka Gitter
* **Currently**: Interim Director of Infrastructure / Gitaly Team Lead / GCP Migration Project Manager, GitLab Inc

[@suprememoocow](https://twitter.com/suprememoocow) on Twitter & GitHub, [@andrewn](https://gitlab.com/andrewn) on GitLab.

^ Hi everyone, my name is Andrew. My friend Joe, who is one of the organisers of this event, but we unable to make it tonight, invited me to come speak to you at ProductTank, to share my story of Gitter, a startup which I helped found in 2012 and worked at until last year.

^ I'm not a product manager, I'm an engineer, but here are a few lessons in Product I learnt along the way.

--------------------------

## What is Gitter?

^ Before I begin, it's probably worth me explaining what Gitter is, and who GitLab are.

![inline](gitter-screenshot.png)

--------------------------

## What is Gitter?

Gitter is a chat and networking platform for software developers

* 1.2M+ registered users
* 400K+ monthly unique users
* 110K+ communities
* 500K+ rooms
* One of the largest GitHub integrations by number of users

[https://gitter.im](https://gitter.im)

^ Gitter is a messaging and community product focused on open-source software development.

^ There are about 1.2M registered users and on any given month, we'll see about 400K unique visitors.

^ There are over 100K communities, operating over half-a-million rooms on the platform

--------------------------

## Who are GitLab?

GitLab is the leading integrated product for modern software development

* [Open-core software](https://about.gitlab.com/2016/07/20/gitlab-is-open-core-github-is-closed-source/) [^1] (Core: open-source, Enterprise: closed-source)
* Over 100K on-premise installs
* GitLab.com: >2M registered users

[https://about.gitlab.com](https://about.gitlab.com)

[^1]: https://about.gitlab.com/2016/07/20/gitlab-is-open-core-github-is-closed-source/

^ GitLab builds tools to help companies develop their own software

^ If you know GitHub, GitLab is a little bit like that, except it's focused on the entire software development process, from conception and planning through to release and monitoring in production. All in the single product.

^ Also, at it's core, it's open-source software, with more than 1900 individual contributors

^ There are over 100K installations, the biggest of which is GitLab.com, which has over 2M registered users

--------------------------

## Who are GitLab?

![25%, inline](gitlab-plan.jpg) ![25%, inline](gitlab-release.jpg)
![25%, inline](gitlab-monitor.jpg)

^ and this is what it looks like...

--------------------------

## Backstory

### Vision: “Really good, modern, messaging for the enterprise”

![inline](hipchat.png)

^ The story of Gitter really starts way back in 2011. My cofounder and long time friend, Mike Bartlett was working as a Product Manager at Skype

^ It was announced that Skype would be acquired by Microsoft

^ Mike had been working there for 7 years and felt that it was a good time to make a break and leave.

^ We'd been friends for a long time, since the late 90's when we both lived here in Cape Town.

^ We would occasionally discuss working together on something. At that time, I was working in the City of London in banking and was looking for something different.

^ Skype was a big player in the consumer messaging space, but Mike had been frustrated with their attempts at moving into the enterprise messaging.

^ Our idea was simple: let's build a _modern_ messaging product for enterprise. A place where teams could collaborate, working on projects together. We wanted to infuse it with an element of fun. Importantly, we wanted to make everything about the experience as frictionless as possible.

^ When we told people our idea, many reminded us that this space was already dominated by products such as Yammer and HipChat, here, but we felt that we could do better, building something more friendly, fun and compelling to use.

^ Mike left Skype and we began collaborating on a product that we called Troupe.

--------------------------

#### Troupe v0.0.1

![inline](troupe-v0.0.1-a.png)

^ This is what the first version of Troupe looked like. It was pretty rough.

^ One thing I want to point out, is that even though it looks pretty terrible, you'll notice the extreme simplicity of the signup page. We tried to make the entire experience as frictionless as possible.

^ When signing up, Troupe asked for the name of your project and your email address. Troupe didn't even ask for your actual name. It would infer it from your email address, but nudge you to update it later on.

^ We encouraged friends to use it for their personal projects.

^ And, of course, we also dog-fooded it ourselves. Using it to collaborate on the development of Troupe itself.

^ Through the course of 2012, we kept iterating on it

^ I still had a full-time job, so I had to work on it after work in the evenings. During this time, my first child was born, so my hands were pretty full juggling things.

^ The home page soon evolved to this...

--------------------------

#### Troupe v0.1.0

![inline](troupe-v0.0.2-home.jpg)

^ If you look closely at the screenshot, you can see the evolution. Within a few months, we supported, chat, email groups, file sharing and status updates.

^ It was still rough as anything, but people were using it every day, filing issues and providing feedback, and it was slowly getting better all the time.

--------------------------

[.background-color: #ffffff]

![fit](index-ventures.png)

^ After working on the project part-time for about 6 months, Mike reached out to an ex-Skype contact at Index Ventures, one of the largest VCs in Europe. We secured a meeting with him to demo what we'd been working on.

^ The engineer-perfectionist in me screamed out for more time to polish things. Surely we wouldn't get funding for this?

^ We met with Index and presented our vision for Troupe and gave them a demo of our rough product.

^ An hour later, we left the meeting having secured enough seed funding to work on Troupe full-time and to hire two more developers with six months of runway.

^ And this brings me to my first lesson...

--------------------------

## Lesson #1

![](soapbox.jpg)

> _“If you are not embarrassed by the first version of your product, you've launched too late”_ -- Reid Hoffman [^2]

[^2]: [Reid Hoffman: https://www.linkedin.com/pulse/arent-any-typos-essay-we-launched-too-late-reid-hoffman/](https://www.linkedin.com/pulse/arent-any-typos-essay-we-launched-too-late-reid-hoffman/)

* aka “Perfect is the enemy of good”

^ Reid Hoffman, a co-founder of LinkedIn put it this way:

^ _“If you are not embarrassed by the first version of your product, you've launched too late”_

^ The first lesson I'd like to share is about releasing early. Really early. Much earlier than you're comfortable with.

^ Put another way, "perfect is the enemy of good"

^ Had we left it another few months months, we almost certainly wouldn't have obtained funding. More on that later.

--------------------------

## Continuing the Build…

### January 2013: Cape Town

[.header: #000000]

![original](tim-johnson-328094-unsplash.jpg)

^ Now that we had some funding, I quit my job.

^ My wife was still on maternity leave and it was winter in London, so we packed our bags and headed to Cape Town to work on the product from here.

^ We hired some desk space in Loop Street, and hired a local contractor to help work on the product.

^ After a few months, we headed back to London, where we hired two full-time engineers to join the company, found some office space and continued building out the product.

^ We had a small but growing user base and we were cautiously optimistic about the future....

^ And then Slack happened...

--------------------------

## “So Yeah We Tried Slack…”

![original](slack.jpg)

^ One day we became of aware of a new startup working on an enterprise messaging product, much like our own. They'd been running in stealth mode for several months before their launch.

^ Out of curiosity, we decided to give it a try. Our inquisitiveness turned to worry: Slack was exactly what we had set out to build with Troupe, except it was far further down the road than ourselves. It was really good too.

^ We had a tiny seed round, and a team of 4, whereas, at that point, they had raised over $16m, they had an engineering staff of probably dozens, and the founder was Stewart Butterfield, a Silicon Valley legend.

^ In the face of this, our initial reaction was denial.

^ Over the course of the next few months, growth dived and it became clear to us that we were not seeing the numbers we needed to build a thriving business.

^ Slack had not only beaten us to market, but they had appeared to have achieved product/market fit too.

^ We started considering a pivot.

^ Up until this point, nearly all of the traction Troupe had gained had been with software developers.

^ Maybe this was because there were a lot of software developers in our networks, I don't know

^ We continually got requests to build features specific to developers, particularly better GitHub integration, but up until that point, we had resisted.

^ That's because we wanted to build a product that was broadly accessible to all enterprise users, not something only for developers.

^ The idea for the pivot that we came up with was to completely repurpose Troupe and rebuild it around Chat for GitHub repositories. We could build the product on top of the GitHub social graph, rather maintaining our own and we would provide deep integration into their product.

^ We socialized the idea with our team and our investors, and then put together a extremely simple one page registration page to gauge if there was any interest in the idea.

--------------------------

## The Pivot

![inline](show-hn-gitter.png)

^ We posted it on HackerNews and it quickly rose to the front page and then into the top position, where it remained for several hours.

^ Many tens of thousands of people registered. In a few hours, we had reached a far greater audience than we had in months of building Troupe.

^ This brings to to my next lesson...

--------------------------

## Lesson #2 (part 1) - Pivoting

### Know when it's time to pivot

^ You may think you've got the best idea in the world, but if your metrics don’t look like a hockey stick… you’ve got it wrong.

^ In your mind, your product might be amazing, but if churn is high and growth is low, then you need a paradigm shift

^ The following day, we made the decision to start building Gitter. Three months later it was released in beta.

^ However, we also decided that we would keep Troupe running.

^ This was partly because we felt the need to look after our early adopters, and partly as an insurance policy in case Gitter failed.

^ However, this decision meant that a small team of four needed to split our time between two products, each with it's own web application, api, iOS app and desktop apps.

^ While we saw widespread adoption of Gitter, Troupe's month-on-month user numbers shriveled.

^ In retrospect, it would have been far better to shut Troupe down immediately, rather than keep it running for a group of several thousand users.

^ This brings me part 2 of this lesson

![](natalia-y-339474-unsplash.jpg)

--------------------------

## Lesson #2 (part 2)

### When you pivot, pivot hard

> _“Burn the boats”_ -- Steve Blank [^3]

  ![original](cortez.png)

^ When you pivot, pivot hard, or as Steve Blank says, Burn the Boats.

^ This is a reference to Hernando Cortes, who in 1519, landed in what is today Mexico in order to attack the Aztec Empire and steal their treasure of gold for Spain. His army arrived in 11 ships. As soon as they had landed Cortes ordered that the ships be burnt.

^ There was no possibility of return.  Now, their only way home was to succeed in their mission or die trying.

^ We should have done the same, giving Troupe users a weeks' grace before shutting the service down.

^ Next time, I'll be far more ruthless about burning all our boats when I pivot

[^3]: [Steve Blank, 2010](https://steveblank.com/2010/09/23/panic-at-the-pivot%E2%80%93aligning-incentives-and-burning-the-boats/)

--------------------------

## Growing the User Base

^ In the months following the the launch of Gitter beta, we experienced solid, if not amazing, growth of around 12% month-on-month on our registered user figures, with a reasonable retention rate.

^ This meant we were growing, just not as much as we wanted

![inline](gitter-prebadger-growth.png)

^ To try boost this growth, we experimented with a series of fairly unoriginal growth-hack techniques, mostly leveraging social media.

^ For example, we made it easy for users to share their communities on Twitter and Facebook and put effort into making public content on Gitter searchable in Google.

^ None of it made much of a difference to our growth figures which remained constant, unfortunately

^ Every Friday the team would meet for a growth-hack brainstorming session, where we would analyse our metrics and look for opportunities to improve growth.

^ In one of these sessions, we were reviewing some data and noticed that GitHub repositories that included a Gitter badge to their Gitter communities had far higher engagement and retention rates than other repositories.

^ To give you an example, here's what a typical badged repository looks like.

--------------------------

^ This screenshot is from one of the Ethereum repositories on GitHub.

^ When you click this badge, you'll navigate to the Gitter community for this Ethereum repository.

^ The problem was, we had to rely on community owners adding the badge to their GitHub repositories. This was something that very few did.

^ What we needed was a way we could do this automatically. Then we realised there was a way. Since these communities were open-source, we build a bot to add the badge, through the GitHub pull request mechanism.

^ It wasn't trivial. We would need to parse the GitHub repository's readme markdown file, in one of many markdown dialects, figure out where the best place to insert the badge is, then submit a pull request to the repository through the GitHub API.

^ Since these were open source repositories, and anybody can send a pull request, we would not require any special permissions in order to do this.

^ The idea was greeted by some of the Gitter team with skepticism. Firstly, we weren't aware of anyone else using GitHub pull requests in this way. We weren't even sure if it could be done technically. Secondly, even if it was possible, there was concern that such an aggressive growth-hack would be viewed badly by the community GitHub users.

^ After much deliberation, we decided to try and build it. This is what it looks like:

![fit](ethereum-badges.png)

--------------------------

![fit](gitter-send-pull-request.png)

^ When you create a new channel on Gitter, this check box allows you to select whether you would like to add the badge to your GitHub repository. By default, it's selected.

![fit](gitter-badger-pull-request.png)

^ This creates a pull request that gets generated on GitHub

--------------------------

## Registered User Month-on-Month Growth for Gitter

![inline](badger-registered-user-mom-growth-inflection.png)

^ The effect was immediate. We released the change on a Friday. Over the weekend we actually needed to scale up our infrastructure in order to cope with extra demand.

--------------------------

## Gitter Badger Results

![](hans-veth-385484-unsplash.jpg)

* Gitter Badge impressions: 6M+ unique users per month
* MoM user growth doubled from 12% to 25%
* 50k+ pull requests generated on GitHub to-date

^ This slide gives you some idea of the effect the badger had on our growth numbers

^ Our badge server serves 6M unique users every month

^ Our month-on-month registered user growth numbers went from 12% to 25%

^ To-date, the badger has created somewhere over 50k pull requests on GitHub

--------------------------

## Badger v. Sharing

![](honey-badger.jpg)

### Share on Twitter/Facebook

* Value to the company
* Ephemeral / once-off

### The Badger

* Value to the user
* Sticky / lasts forever
* Effect amplifies over time

^ The growth techniques we had focused on up until that point didn't really add a lot of value to our users

^ What's more, it was likely that most users would only share their community on social media once. It was ephemeral.

^ The badger, in contrast, added value for our users. With one click, their new Gitter community could be linked to an existing repository. This saved the user time and also gave us a permanent presence from their repository.

--------------------------

## Lesson #3

### Tailor your growth hacking to approaches that are natural to your audience

* Look for growth-hack techniques that add value for your users
* Don't be afraid of aggressive growth hacks

^ The lesson is: whenever possible, tailor your growth hacking to approaches that are natural to your audience and in ways that adds value for your users, rather than only for your startup.

^ We were spending effort trying to growth hack on social media, but primarily, our users spent their time on GitHub.

^ Once we focused on this, the results were immediate.

^ It's also worth noting that the backlash that we had feared from our users never materialised. There were a few complaints, but nearly all of the feedback we received was overwhelmingly positive.

^ That's my final lesson. Really all that's left is to complete the story with GitLab's acquisition of Gitter...

--------------------------

## Acquisition

![inline fit](gitlab-acquires-gitter.png)

^ In December 2016, GitLab approached us with an acquisition offer.

^ We were impressed by the company, it's strategy and it's particularly it open nature. We accepted the offer and some of the team, including both founders, joined GitLab

--------------------------

## Current Role: GCP Migration

![inline fit](gitlab-gcp.png)

^ I've chosen to stay on at GitLab. I'm currently running the project to migrate GitLab's entire cloud infrastructure from Microsoft Azure Cloud to Google's Cloud Platform, and then eventually to Kubernetes using Google Container Engine.

^ That's right, the same Google Cloud Platform who are sponsoring this event tonight!

^ While I'm talking about GitLab, I must add that that,

--------------------------

## GitLab is Hiring!

#### In Cape Town, Joburg (and almost everywhere else)

![](gitlab_team_summit_greece_2017.png)

**Product Managers, Engineers, Security Analysts, Support, Designers, Account Managers**

[https://about.gitlab.com/jobs](https://about.gitlab.com/jobs/#vacancies)

^ GitLab is hiring. We're looking for product Managers, engineers, security analysts, support engineers, UX specialists, designers, account managers, you name it.

^ Right now, we're hiring in Cape Town, Joburg, Bloemfontein and Durban. In fact, we're hiring anywhere that there's decent internet connection. That's because GitLab is a totally remote company and you can work for them from wherever you chose.

^ If you're thinking about looking for a new role, I strongly encourage you to take a look at our jobs page, or speak to myself or my colleague Collen who is here in the audience.

--------------------------

## Recap: Lessons

1. If you are not embarrassed by the first version of your product, you've launched too late

2. Know when it's when it's time to pivot

3. When you pivot, pivot hard ("Burn the Boats")

4. Tailor your growth hacking to approaches that are natural to your audience

^ Finally, let's conclude by recapping the lessons that I learnt...

--------------------------

## Questions?
